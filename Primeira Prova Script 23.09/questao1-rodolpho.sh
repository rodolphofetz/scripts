echo "Quanto mais avança a tecnologia, mais respeitado será o Ser que se mantiver Humano."
sleep 2
echo "A tecnologia aproxima quem está longe e afasta quem está perto."
sleep 2
echo "Uma máquina consegue fazer o trabalho de 50 homens ordinários. Nenhuma máquina consegue fazer o trabalho de um homem extraordinário."
sleep 2
echo "É divertido fazer o impossível, pois lá a concorrência é menor."
sleep 2
echo "O $ move o mundo, mas não move quem o criou."
